// Date class definition; Member functions defined in Date.cpp

#ifndef DATE_H
#define DATE_H

#include <iostream>

class Date
{
public:
    Date();
    Date(int, int, int);
    void setDay(int);
    void setMonth(int);
    void setYear(int);

    int getDay() const;
    int getMonth() const;
    int getYear() const;
    bool isOldEnough() const;
    int checkDay(int) const;
    int checkMonth(int) const;
    int checkYear(int) const;
    std::string printDate() const;
    
private:
    int day, month, year;
};

#endif
