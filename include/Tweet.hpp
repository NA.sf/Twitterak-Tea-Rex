// Tweet class definition; Member functions defined in Tweet.cpp

#ifndef TWEET_H
#define TWEET_H

#include <iostream>
#include <string>
#include <vector>

#include "Date.hpp"

class User;
class Tweet
{
public:
    Tweet ();
    void setText(std::string &);
    std::string getText();
    void setLike(User*);
    int getSize();
    void seDislike(User *);
    void showLike();

private:
    std::vector<User *> likes;
    Date date;
    std::string text;
};

#endif
