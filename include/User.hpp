// User class definition; Member functions defined in User.cpp

#ifndef USER_H
#define USER_H

#include <iostream>
#include <string>
#include <vector>

#include "Tweet.hpp"
#include "Date.hpp"

class User
{
public:
    User(int, int, int);
    void setUsername(std::string, const std::vector<User *> &);
    void setName(std::string);
    void setBiography(std::string);
    void setCountry(std::string);
    void setLink(std::string);
    void setPhoneNumber(std::string, const std::vector<User *> &);
    void setPassword(const std::string &);
    void setHeader(std::string);
    void setBirthDay(int);
    void setBirthMonth(int);
    void setBirthYear(int);

    std::string getUsername() const;
    std::string getName() const;
    std::string getBiography() const;
    std::string getCountry() const;
    std::string getLink() const;
    std::string getPhoneNumber() const;
    std::string getPassword() const;
    std::string getHeader() const;
    std::string getJoinDate() const;
    std::string getBirthDate() const;
    std::string showColor() const; // For showing header's color

    bool isOld() const; // For edit tweet
    std::vector<Tweet *> getVecTweet(); // User's tweets
    void deleteTweet(int);
    void changeTweet(int, std::string &);
    void setVec(std::string &); // Adds new tweet

private:
    std::string name, username, biography, country, link, phoneNumber, password, header;
    Date birthDate;
    std::vector <Tweet *> vecTweet;
    const Date joinDate;
};

#endif
