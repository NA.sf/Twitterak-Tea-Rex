// Twitterak class definition; Member functions defined in Twitterak.cpp

#ifndef TWITTERAK_H
#define TWITTERAK_H

#include <iostream>

#include "User.hpp"
#include "Tweet.hpp"

class Twitterak
{
public:
    void run();
    void help();
    void helpLogin();
    void checkLogin(std::string &, std::string &); // Check password and username
    void login(User *);
    void signup(std::string &);
    void showProfileMe(const User *) const;
    void showProfile(const std::string &) const;
    void editProfile(User*, const std::string&, std::string&);

private:
    std::vector<User *> users;
};

#endif
