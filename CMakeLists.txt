cmake_minimum_required(VERSION 3.0)

# set(CMAKE_CXX_COMPILER C:/msys64/mingw64/bin/g++.exe)
# set(CMAKE_C_COMPILER C:/msys64/mingw64/bin/gcc.exe)

project(Twitterak)
set(CMAKE_CXX_STANDARD 11)
set(SOURCES
	src/main.cpp
	src/Twitterak.cpp
	src/User.cpp
	src/Tweet.cpp
	src/Date.cpp
	src/Config.cpp
	hash-library/sha256.cpp
)
include_directories(include hash-library/)
add_executable(app ${SOURCES})
