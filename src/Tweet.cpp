// Tweet class member-function definitions.

#include <iostream>
#include <string>
#include <vector>

#include "Date.hpp"
#include "Tweet.hpp"
#include "User.hpp"
#include "Config.hpp"

Tweet::Tweet()
{

}
void Tweet::setText(std::string & input )
{
    this->text = input;
}
std::string Tweet::getText()
{
    return this->text;
}
void Tweet::setLike(User * user)
{
    for (User * liker : likes)
    {
        if (liker == user)
        {
            std::cout << RED << "\t! You have liked this tweet." << RESET << "\n";
            return;
        }
    }
    this->likes.push_back(user);

}
int Tweet::getSize()
{
    return this->likes.size();
}
void Tweet::seDislike(User * user)
{
    int pos = 0;
    for (User * liker :likes)
    {
        if (liker == user)
        {
            likes.erase(likes.begin() + pos);
            return;
        }
        pos++;
    }
    std::cout << RED << "\t! You haven't liked this tweet.\n" << RESET;
}
void Tweet::showLike()
{
    if (likes.size() == 0) std::cout << RED << "\t! Nobody has liked this tweet.\n" << RESET;
    else
    {
        for(int liker = 0; liker < likes.size(); liker++)
        {
            try
            {
                std::cout << "\t"<< this->likes.at(liker)->getUsername() << "\n";
            }
            catch (std::bad_alloc&) {}
        }
    }
}
