// Date class member-function definitions.

#include <iostream>
#include <ctime>
#include <stdexcept>
#include <sstream>

#include "Date.hpp"

Date::Date() : day{0}, month{0}, year{0}
{
}

Date::Date(int dd, int mm, int yy) : day{dd}, month{mm}, year{yy}
{
}

void Date::setDay(int inputDay)
{
   this->day = checkDay(inputDay);
}

void Date::setMonth(int inputMonth)
{
   this->month = checkMonth(inputMonth);
}

void Date::setYear(int inputYear)
{
   this->year = checkYear(inputYear);
}

int Date::getDay() const
{
   return this->day;
}

int Date::getMonth() const
{
   return this->month;
}

int Date::getYear() const
{
   return this->year;
}

bool Date::isOldEnough() const
{
   time_t now = time(0);
   int Year = 1900 + localtime(&now)->tm_year;
   if ((Year - this->year) < 18)
      return false;
   else
      return true;
}

int Date::checkDay(int inputDay) const
{
   time_t now = time(0);
   static const int daysPerMonth[12] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
   if (inputDay > 0 && inputDay <= daysPerMonth[month - 1])
   {
      if (!((this->year == 1900 + localtime(&now)->tm_year) && (this->month == 1 + localtime(&now)->tm_mon) && (inputDay > localtime(&now)->tm_mday)))
      {
         return inputDay;
      }
   }
   if (month == 2 && inputDay == 29 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)))
   {
      return inputDay;
   }
   throw std::invalid_argument("\t! Invalid day");
}

int Date::checkMonth(int inputMonth) const
{
   time_t now = time(0);
   if (inputMonth > 0 && inputMonth < 13)
   {
      if (!((this->year == 1900 + localtime(&now)->tm_year) && (inputMonth > 1 + localtime(&now)->tm_mon)))
      {
         return inputMonth;
      }
   }
   throw std::invalid_argument("\t! Invalid month");
}

int Date::checkYear(int inputYear) const
{
   time_t now = time(0);
   if (inputYear > 1902 && inputYear <= 1900 + localtime(&now)->tm_year)
   {
      return inputYear;
   }
   throw std::invalid_argument("\t! Invalid year");
}

std::string Date::printDate() const
{
   std::ostringstream output;
   switch (this->month)
   {
      case 1:
         output << "Jan ";
         break;
      case 2:
         output << "Feb ";
         break;
      case 3:
         output << "Mar ";
         break;
      case 4:
         output << "Apr ";
         break;
      case 5:
         output << "May ";
         break;
      case 6:
         output << "Jun ";
         break;
      case 7:
         output << "Jul ";
         break;
      case 8:
         output << "Aug ";
         break;
      case 9:
         output << "Sep ";
         break;
      case 10:
         output << "Oct ";
         break;
      case 11:
         output << "Nov ";
         break;
      case 12:
         output << "Dec ";
         break;
   }
   output << this->day << ", " << this->year;
   return output.str();
}
