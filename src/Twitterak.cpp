// Twitterak class member-function definitions.

#include <iostream>
#include <string>
#include <unistd.h>
#include <iomanip>
#include <algorithm>
#include <sstream>

#include "sha256.h"
#include "Twitterak.hpp"
#include "User.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"

void Twitterak::run()
{
    system("clear || cls");
    std::cout << "\n\n\n\tWelcome to Twitterak, version Tea-Rex 1.1\n";
    std::string input;
    while (true)
    {
        std::cout << "\t> ";
        getline(std::cin, input);
        system("clear || cls");
        std::string inputCopy = input;
        std::transform(inputCopy.begin(), inputCopy.end(), inputCopy.begin(), ::tolower);
        setString(inputCopy);
        if (inputCopy == "help")
        {
            help();
        }
        else if ((inputCopy.size() > 5) && (inputCopy.substr(0, 6) == "signup"))   // signup
        {                                                                          // ^    ^
            std::string userName = "";                                             // 0    5
            if (inputCopy.size() > 11)                                                                                                                                                                                                                                                                                                                                                                  
            {
                userName = inputCopy.substr(7, inputCopy.size() - 7);
            }
            signup(userName);
            // system("clear || cls");
            continue;
        }
        else if ((inputCopy.size() > 4) && (inputCopy.substr(0, 5) == "login"))
        {
            std::string userName = "", passWord = "";
            if (inputCopy.size() > 10)
            {
                if (inputCopy.find(' ', 10) != std::string::npos)
                {
                    userName = inputCopy.substr(6, inputCopy.find(' ', 10) - 6);
                    passWord = input.substr(input.find(' ', 10) + 1, input.size() - input.find(' ', 10));
                }
                else userName = inputCopy.substr(6, inputCopy.size() - 6);
            }
            system("clear || cls");
            checkLogin(userName, passWord);
            continue;
        }
        else if (input == "exit" || input == "quit" || input == "q")
        {
            return;
        }
        else if (input == "cls" || input == "clear")
        {
            system("clear || cls");
        }
        else if (input != "")
        {
            std::cout << RED << "\t! Command Not Found.\n" << RESET;
        }
    }
}

void Twitterak::help()
{
    std::cout << "\t> help\n\t" << std::left
              << std::setw(8) <<  "Signup" << std::setw(23) << "[Username]" << "Create an account\n\t"
              << std::setw(8) <<  "Login"  << std::setw(23) << "[Username] [Password]" << "Sign in to your account\n\t"
              << std::setw(8) <<  "Exit"   << std::setw(23) << "" << "Leave Twitterak\n";
}

void Twitterak::helpLogin()
{
    std::cout << "\t> help\n\t" << std::left
              << std::setw(16) <<  "Delete account" << std::setw(28) << "" << "Delete your account\n\t"
              << std::setw(16) <<  "Profile or Me"  << std::setw(28) << "" << "Show your information\n\t"
              << std::setw(16) <<  "Profile"        << std::setw(28) << "<Username>" << "Show a user's public information\n\t"
              << std::setw(16) <<  "Edit profile"   << std::setw(28) << "<Field> <New Data>" << "Change your account's information\n\t"
              << std::setw(16) <<  "Tweet"          << std::setw(28) << "<Tweet text>" << "Tweet a text\n\t"
              << std::setw(16) <<  ""               << std::setw(28) << "<@Username>" << "Show a user's tweets\n\t"
              << std::setw(16) <<  "Delete tweet"   << std::setw(28) << "<TweetID>" << "Dlete a tweet\n\t"
              << std::setw(16) <<  "Edit tweet"     << std::setw(28) << "<TweetID>" << "Edit a tweet\n\t"
              << std::setw(16) <<  ""               << std::setw(28) << "<Username>:<TweetID>:likes" << "Show who has likes a tweet\n\t"
              << std::setw(16) <<  "Like"           << std::setw(28) << "<Username>:<TweetID>" << "Like a tweet\n\t"
              << std::setw(16) <<  "Dislike"        << std::setw(28) << "<Username>:<TweetID>" << "Dislike a tweet\n\t"
              << std::setw(16) <<  "Logout"         << std::setw(28) << "" << "Logout from your account\n";
}

void Twitterak::checkLogin(std::string & userName, std::string & passWord)
{
    SHA256 sh;
    if (userName != "")                                                                                                              // login (@)username password
    {
        for (User * foundUser : users)
        {
            std::cout << foundUser->getUsername() << '\n' ;
            if (foundUser->getUsername() == userName)
            {
                if (passWord != "")
                {
                    while(foundUser->getPassword() != sh(passWord))
                    {
                        std::cout << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << RED <<
                        "\n\t! Password isn't correct." << RESET << "\n\tPassword: " << HIDDEN;
                        getline(std::cin, passWord, '\n');
                    }
                    login(foundUser);
                    return;
                }
                else                                                                                                                    // login (@)username
                {
                    std::cout << "\n\tPassword: " << HIDDEN;
                    getline(std::cin, passWord, '\n');
                    while(foundUser->getPassword() != sh(passWord))
                    {
                        std::cout << RESET  << RESET << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP <<
                        RED << "\n\t! Password isn't correct." << RESET << "\n\tPassword: " << HIDDEN;
                        getline(std::cin, passWord, '\n');
                    }
                    login(foundUser);
                    return;
                }
            }
        }
        std::cout << LINEUP << LINEUP << DELLINE << RED << "\n\t! User not found." << RESET;
    }
    while (true)                                                                                                                           // login
    {
        std::cout << "\n\tUsername: ";
        getline(std::cin, userName);
        for (User * foundUser : users)
        {
            if (foundUser->getUsername() == userName)
            {
                std::cout << LINEUP << LINEUP << DELLINE << "\n\n\n\tPassword: ";
                getline(std::cin, passWord);
                while(foundUser->getPassword() != sh(passWord))
                {
                    std::cout << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << RED <<
                    "\n\t! Password isn't correct." << RESET << "\n\tPassword: " << HIDDEN;
                    getline(std::cin, passWord, '\n');
                }
                login(foundUser);
                return;
            }
        }
        std::cout << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << DELLINE << RED << "\n\t! User not found." << RESET;
    }
}

void Twitterak::login(User * theUser)
{
    while (true)
    {
        std::cout << RESET << "\t> " << theUser->showColor() << "@" << theUser->getUsername() << RESET << " > ";
        std::string input, inputCopy;
        getline(std::cin, input);
        inputCopy = input;
        std::transform(input.begin(), input.end(), input.begin(), ::tolower);
        setString(input);
        if (input == "delete account")                                                          //check for delete account
        {
            std::cout << "\n\t? This operation cannot be reversed in any away.Are you sure?(y/n) ";
            std::string yorn;
            std::cin >> yorn;
            if (yorn == "y")
            {
                int pos = 0;
                for (User * i : users)
                {
                    if (i->getUsername() == theUser->getUsername())
                    {
                        delete users[pos];
                        users.erase(users.begin() + pos);
                        std::cout << GREEN << "\t* Your account was successfully deleted.\n" << RESET;
                        return;
                    }
                    pos++;
                }
            }
            else if (yorn == "n")
            {
                continue;
            }
        }
        else if (input == "logout")
        {
            system("clear || cls");
            std::cout << GREEN << "\t* You have successfully logged out.\n" << RESET;
            return;
        }
        else if (input == "help")
        {
            helpLogin();
            continue;
        }
        else if ((input == "profile") || (input == "me"))                                           //check for show your profile
        {
            showProfileMe(theUser);
        }

        else if ((input.size() > 13) && (input.substr(0, 7) == "profile"))                       //check for show others profile
        {
            std::string user = input.substr(8, input.size() - 8);
            showProfile(user);
        }

        else if ((input.size() > 20) && (input.substr(0, 12) == "edit profile"))                  //check for edit profile
        {
            if (input.find("\"") == std::string::npos)
            {
                std::string str = input.substr(13, 11);                                                   
                std::string str1 = input.substr(25,input.size()-25);                             //check for phonenumber without ""       
                editProfile(theUser, str, str1);
            }
            else
            {
                int num = input.find("\"") - 1;
                std::string str = input.substr(13, num - 13);                                                  // like country
                std::string str1 = input.substr(num + 2, input.size() - num - 3);                              // like france
                editProfile(theUser, str, str1);
            }
        }
        else if ((input.size() > 7) && (input.substr(0, 5) == "tweet"))                                       //tweet comment
        {
            if (((input.find("\"",input.find("\"")+1)) != std::string::npos))
            {
                std::string str = input.substr(7, input.size() - 8);
                theUser->setVec(str);
            }
            else
            {
                std::string str = input.substr(6, input.size() - 6);
                theUser->setVec(str);
            }
        }
        else if ((input.size() > 13) && (input.substr(0, 12) == "delete tweet"))                        //delete tweet
        {
            int sum = 0;
            std::string st1 = input.substr(13,input.size()-13);
            std::stringstream  out1;
            out1 << st1;
            out1 >> sum;
            if ((theUser->getVecTweet()[sum-1] != nullptr) && (sum-1) < theUser->getVecTweet().size())
            {
                theUser->deleteTweet(sum-1);
                std::cout << GREEN << "\t* Your tweet was deleted.\n" << RESET;
            }
            else 
            {
                std::cout << RED << "\t! Tweet with this ID doesn't exist.\n" << RESET;
            }
        }
        else if ((input.size() > 11) && (input.substr(0, 10) == "edit tweet"))                              //edit tweet
        {
            if (theUser->isOld())
            {
                int sum1 = 0;
                std::string st = input.substr(11,input.size()-11);
                std::stringstream  out;
                out << st;
                out >> sum1;
                if ((theUser->getVecTweet()[sum1-1] != nullptr) && ((sum1-1) < theUser->getVecTweet().size()))
                {
                    std::cout << "\t"<< sum1 << ": " << theUser->getVecTweet()[sum1-1]->getText();
                    std::cout << "\n\t* Enter new text for tweet " << sum1 << ": ";
                    std::string str;
                    getline(std::cin, str);
                    theUser->changeTweet(sum1, str);
                }
                else 
                {
                    std::cout << RED << "\t! Tweet with this ID doesn't exist.\n" << RESET;
                }
            }
            else
            {
                std::cout << RED << "\n\t! You must be at least 18 years old." << RESET;
            }
        }
        else if(input.find(":",input.find(":") + 1) != std::string::npos )                   // shows how many likes does a tweet has and who has liked that
        {
            std::string str = input.substr(0, input.find(":"));                              // find username
            int num = input.find(":", input.find(":") + 1);                                  // for find second ":"
            std::string str2 = input.substr(input.find(":") + 1, num - 1 - input.find(":")); // for find number of tweet
            std::stringstream ss;
            ss << str2;
            int num2;
            ss >> num2;
            bool exist = true;
            for(User * foundUser : users)
            {
                if (foundUser->getUsername() == str)
                {
                    exist = false;
                    if (((num2-1) < (foundUser->getVecTweet().size())) && (foundUser->getVecTweet().at(num2-1) != nullptr))
                    {
                        foundUser->getVecTweet().at(num2-1)->showLike();
                    }
                    else
                    {
                        std::cout << RED << "\t! Tweet with this ID doesn't exist.\n" << RESET;
                    }
                }
            }
            // std::cout << input << "\n";
            if (exist == true)
            {
                std::cout << RED << "\t! User not found.\n" << RESET;
            }
        }
        else if ((input.size() > 2) && ((input.substr(0, 3) == "me:") || ((input.find(":") != std::string::npos) && (input.substr(0, input.find(":") - 1) == theUser->getUsername()))))
        {
            std::stringstream ss;
            int tweetNum;
            if (input.size() - input.find(":") - 1 != 0)
            {
                ss << input.substr(input.find(":") + 1, input.size() - input.find(":") - 1);
                ss >> tweetNum;
                if ((tweetNum != 0) && (tweetNum <= theUser->getVecTweet().size()) && (theUser->getVecTweet()[tweetNum-1] != nullptr))
                {
                    std::cout << "\t" << tweetNum << ": "<< theUser->getVecTweet()[tweetNum - 1]->getText() << " : " << theUser->getVecTweet()[tweetNum - 1]->getSize() << "\n";
                }
                else
                {
                    std::cout << RED << "\t! There is not a tweet with this ID.\n" << RESET;
                }
            }
            else
            {
                std::cout << RED << "\t! Command not found.\n" << RESET;
            }
        }
        else if ((input == "@me") || (input == "@" + theUser->getUsername()))
        {
            for (int tweetID = 0; tweetID < theUser->getVecTweet().size(); tweetID++)
            {
                if (theUser->getVecTweet()[tweetID] != nullptr)
                {
                    std::cout << "\t" << tweetID + 1 << ": " << theUser->getVecTweet()[tweetID]->getText() << " : " << theUser->getVecTweet()[tweetID]->getSize() << "\n";
                }
            }
        }
        else if (input.substr(0, 4) == "like")
        {
            std::string str = input.substr(5, input.find(":") - 5);
            std::string str1 = input.substr(input.find(":") + 1, input.size() - input.find(":") - 1);
            std::stringstream s;
            // std::cout << str << "\t" << str1 << "\n";
            s << str1;
            int sum2;
            s >> sum2;
            bool exist = false;
            for(User * foundUser : users)
            {
                if (foundUser->getUsername() == str)
                {
                    exist = true;
                    if (((sum2-1) < (foundUser->getVecTweet().size())) && (foundUser->getVecTweet().at(sum2 - 1) != nullptr))
                    {
                        foundUser->getVecTweet().at(sum2-1)->setLike(theUser);               //user has vector of tweet that every tweet has vector of user
  
                    }
                    else
                    {
                        std::cout << RED << "\t! This tweet not exist.\n" << RESET;
                    }
                }
            }
            if (!exist)
            {
                std::cout << RED << "\t! User not found.\n" << RESET;
            }
        }
        else if (input.substr(0,7) == "dislike")
        {
            std::string str = input.substr(8, input.find(":") - 8);
            std::string str1 = input.substr(input.find(":") + 1, input.size()-input.find(":") + 1);
            std::stringstream s;
            s << str1;
            int sum2;
            s >> sum2;
            bool exist = true;
            for (User * foundUser : users)
            {
                if (foundUser->getUsername() == str)
                {
                    exist = false;
                    if (((sum2 - 1) < (foundUser->getVecTweet().size())) && (foundUser->getVecTweet().at(sum2 - 1) != nullptr))
                    {
                        foundUser->getVecTweet().at(sum2-1)->seDislike(theUser);
                    }
                    else
                    {
                        std::cout << RED << "\t! This tweet not exist.\n" << RESET;
                    }
                }
            }
            if (exist == true)
            {
                std::cout << RED << "\t! User not found.\n" << RESET;
            }
        }
        else if ((input.find(":") != std::string::npos) && (input.find(":") > 5) && (input.size() - input.find(":") - 1 != 0))
        {
            for (User * foundUser : users)
            {
                if (foundUser->getUsername() == input.substr(0, input.find(":") - 1))
                {
                    std::stringstream ss;
                    int tweetNum;
                    ss << input.substr(input.find(":") + 1, input.size() - input.find(":") - 1);
                    ss >> tweetNum;
                    if ((tweetNum != 0) && (tweetNum <= foundUser->getVecTweet().size()) && (theUser->getVecTweet()[tweetNum-1] != nullptr))
                    {
                        std::cout << "\t" << tweetNum << ": " << foundUser->getVecTweet()[tweetNum - 1]->getText() << " : " << foundUser->getVecTweet()[tweetNum - 1]->getSize() << "\n";
                        continue;
                    }
                    else
                    {
                        std::cout << RED << "\t! Tweet with this ID doesn't exist.\n" << RESET;
                        continue;
                    }
                }
            }
            std::cout << RED << "\t! User not found.\n" << RESET;
        }
        else if ((input.find(":") == std::string::npos) && (input.size() > 4))
        {
            bool found = false;
            for (User * foundUser : users)
            {
                if (foundUser->getUsername() == input.substr(1, input.size() - 1))
                {
                    for (int tweetID = 0; tweetID < foundUser->getVecTweet().size(); tweetID++)
                    {
                        std::cout << "\t" << tweetID + 1 << ": " << foundUser->getVecTweet()[tweetID]->getText() << " : " << foundUser->getVecTweet()[tweetID]->getSize() << "\n";
                    }
                    found = true;
                }
            }
            if (!found) std::cout << RED << "\t! User not found.\n" << RESET;
        }
        else if (input == "cls" || input == "clear")
        {
            system("clear || cls");
        }
        else if (input != "")
        {
            std::cout << RED << "\t! Command not found.\n" << RESET;
        }
    }
}

void Twitterak::signup(std::string & userName)
{
    system("clear || cls");
    time_t now = time(0);
    User theUser(1900 + localtime(&now)->tm_year, 1 + localtime(&now)->tm_mon, localtime(&now)->tm_mday);
    std::string input;
    std::cout << std::endl;
    bool moreInput = true;                                      // true till name is filled
    if (userName != "")
    {
        try
        {
            theUser.setUsername(userName, users);               //exception for username
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << RED << ex.what() << RESET;
            userName = ";";
        }
    }
    if (theUser.getUsername() == "") do
    {
        try
        {
            while (true)
            {
                std::cout << std::endl << DELLINE << "\t$ Username: ";
                getline(std::cin, input, '\n');
                std::transform(input.begin(), input.end(), input.begin(), ::tolower);
                if (input != "" || input.size() != 0)
                {
                    std::cout << LINEUP << LINEUP << DELLINE;
                    theUser.setUsername(input, users);
                    break;
                }
                else
                {
                    std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << RED << ex.what() << RESET;
        }
    } while (moreInput);
    if (userName == ";" || userName == "") std::cout << std::endl << std::endl;
    while (true)
    {
        std::cout << "\n\t$ Name: ";
        getline(std::cin, input);
        setString(input);
        if (input != "" || input.size() != 0)
        {
            std::cout << LINEUP << LINEUP << DELLINE;
            break;
        }
        else
        {
            std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET;
        }
    }
    theUser.setName(input);
    moreInput = true;                                       // false if set biography
    std::cout << std::endl
              << std::endl;
    do
    {
        std::cout << "\n\t$ Biography: ";                   //exception for biography if more than 160 words
        getline(std::cin, input);
        try
        {
            theUser.setBiography(input);
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << DELLINE << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << RED << ex.what() << RESET;
        }
    } while (moreInput);
    std::cout << "\n\t$ Country: ";
    getline(std::cin, input);
    theUser.setCountry(input);
    std::cout << "\n\t$ Link: ";
    getline(std::cin, input);
    setString(input);
    theUser.setLink(input);
    moreInput = true;
    do
    {
        try
        {
            while (true)
            {
                std::cout << std::endl << DELLINE << "\t$ PhoneNumber: ";
                getline(std::cin, input);
                setString(input);
                if (input != "" || input.size() != 0)
                {
                    std::cout << LINEUP << LINEUP << DELLINE;
                    theUser.setPhoneNumber(input, users);
                    break;
                }
                else
                {
                    std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << RED << ex.what() << RESET;
        }
    } while (moreInput);
    std::cout << std::endl
              << std::endl;
    moreInput = true;
    do
    {
        try
        {
            while (true)
            {
                std::cout << std::endl << DELLINE << "\t$ Password: " << HIDDEN;
                getline(std::cin, input, '\n');
                if (input != "" || input.size() != 0)
                {
                    std::cout << RESET << LINEUP << LINEUP << DELLINE;
                    theUser.setPassword(input);
                    break;
                }
                else
                {
                    std::cout << RESET << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << RED << ex.what() << RESET;
        }
    } while (moreInput);
    theUser.setPassword(input);
    std::cout << RESET << std::endl << std::endl;
    moreInput = true;
    do
    {
        std::cout << "\n\t$ Header: ";
        getline(std::cin, input);
        try
        {
            theUser.setHeader(input);
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << LINEUP << DELLINE << LINEUP << DELLINE << RED << ex.what() << RESET;
        }
    } while (moreInput);
    std::cout << LINEUP << LINEUP << DELLINE << std::endl << std::endl;
    int number;
    moreInput = true;
    do
    {
        try
        {
            while (true)
            {
                std::cout << std::endl << DELLINE << "\t$ Birth Date >   Year: ";
                getline(std::cin, input);
                if (input != "" || input.size() != 0)
                {
                    std::stringstream ss;
                    std::cout << LINEUP << LINEUP << DELLINE;
                    ss << input;
                    ss >> number;
                    theUser.setBirthYear(number);
                    break;
                }
                else
                {
                    std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << RED << ex.what() << RESET;
        }
    } while (moreInput);
    moreInput = true;
    std::cout << DELLINE << LINEDN;
    do
    {
        try
        {
            while (true)
            {
                std::cout << COLUMNMON << "Month: " << CLRLINE;
                getline(std::cin, input);
                if (input != "" || input.size() != 0)
                {
                    std::stringstream ss;
                    std::cout << LINEUP << LINEUP << DELLINE;
                    ss << input;
                    ss >> number;
                    theUser.setBirthMonth(number);
                    break;
                }
                else
                {
                    std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET << BEGLINE;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << LINEUP << LINEUP << DELLINE << std::endl << std::endl << RED << ex.what() << RESET << BEGLINE;
        }
    } while (moreInput);
    moreInput = true;
    std::cout << DELLINE << LINEDN;
    do
    {
        try
        {
            while (true)
            {
                std::cout << COLUMNDAY << "Day: " << CLRLINE;
                getline(std::cin, input);
                if (input != "" || input.size() != 0)
                {
                    std::stringstream ss;
                    std::cout << LINEUP << LINEUP << DELLINE;
                    ss << input;
                    ss >> number;
                    theUser.setBirthDay(number);
                    break;
                }
                else
                {
                    std::cout << LINEUP << LINEUP << DELLINE << RED << "\t! This field is required." << RESET << BEGLINE;
                }
            }
            moreInput = false;
        }
        catch (std::invalid_argument &ex)
        {
            std::cout << LINEUP << LINEUP << DELLINE << std::endl << std::endl << RED << ex.what() << RESET << BEGLINE;
        }
    } while (moreInput);
    system("clear || cls");
    std::cout << GREEN << "\t* Registration was successful.\n" << RESET;
    users.push_back(new User(theUser));
    login(users[users.size() - 1]);
}

void Twitterak::showProfileMe(const User * theUser) const                          // funtion that shows your profile
{
    std::cout << GRAY <<  "\tUsername: "     << RESET << theUser->getUsername()
              << GRAY << "\n\tName: "        << RESET << theUser->getName()
              << GRAY << "\n\tBioghraphy: "  << RESET << theUser->getBiography()
              << GRAY << "\n\tCountry: "     << RESET << theUser->getCountry()
              << GRAY << "\n\tLink: "        << RESET << theUser->getLink()
              << GRAY << "\n\tPhoneNumber: " << RESET << theUser->getPhoneNumber()
              << GRAY << "\n\tHeader: "      << RESET << theUser->getHeader()
              << GRAY << "\n\tBirth date: "  << RESET << theUser->getBirthDate()
              << GRAY << "\n\tJoin date: "   << RESET << theUser->getJoinDate() << "\n";
}

void Twitterak::showProfile(const std::string& input) const                         //function that shows other user's profile
{
    bool use = true;
    for (User * i : users)
    {
        if (i->getUsername() == input)
        {
            use = false;
            std::cout << GRAY << "\tUsername: "     << RESET << i->getUsername()
                      << GRAY << "\n\tName: "       << RESET << i->getName()
                      << GRAY << "\n\tBioghraphy: " << RESET << i->getBiography()
                      << GRAY << "\n\tCountry: "    << RESET << i->getCountry()
                      << GRAY << "\n\tLink: "       << RESET << i->getLink()
                      << GRAY << "\n\tHeader: "     << RESET << i->getHeader()
                      << GRAY << "\n\tJoin date: "  << RESET << i->getJoinDate() << "\n";
        }
    }
    if (use == true)
    {
        std::cout << RED << "\tThe user wasn't found.\n" << RESET;
    }
}

void Twitterak::editProfile(User * user, const std::string & input, std::string & input1)    //function for edit profile
{
    if (input == "name")
    {
        user->setName(input1);
    }
    else if (input == "username")
    {
        std::transform(input1.begin(), input1.end(), input1.begin(), ::tolower);
        try
        {
            user->setUsername(input1, users);
        }
        catch(std::invalid_argument & ex)
        {
            std::cout << RED << ex.what() << RESET << '\n';
            return;
        }
    }
    else if (input == "country")
    {
        user->setCountry(input1);
    }
    else if (input == "bioghraphy")
    {
        user->setBiography(input1);
    }
    else if (input == "phonenumber")
    {
        try
        {
            user->setPhoneNumber(input1, users);
        }
        catch(std::invalid_argument & ex)
        {
            std::cout << '\t' << RED << ex.what() << RESET << '\n';
            return;
        }
    }
    else if (input == "link")
    {
        user->setLink(input1);
    }
    else if (input == "header")
    {
        user->setHeader(input1);
    }
    else if (input == "password")
    {
        SHA256 sh;
        std::string passWord;
        std::cout << "\tPlesse enter your current password: " << HIDDEN;
        getline(std::cin, passWord, '\n');
        while(user->getPassword() != sh(passWord))
        {
            std::cout << RESET << LINEUP << DELLINE << LINEUP << DELLINE << LINEUP << RED <<
            "\n\t! Password isn't correct." << RESET << "\n\tPassword: " << HIDDEN;
            getline(std::cin, passWord, '\n');
        }
        user->setPassword(input1);
        std::cout << RESET;
    }
    else
    {
        std::cout << RED << "! Invalid argument.\n" << RESET;
        return;
    }
    std::cout << GREEN << "\t* Your " << input << " has been successfully changed.\n" << RESET;
}
