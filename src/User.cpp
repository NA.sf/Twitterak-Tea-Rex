// User class member-function definitions.

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <ctime>

#include "sha256.h"
#include "User.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"

User::User(int joinYear, int joinMonth, int joinDay) : name{""}, username{""}, biography{""}, country{""}, link{""}, phoneNumber{""}, password{""}, header{"white"}
, joinDate(joinYear, joinMonth, joinDay) 
{
}

void User::setUsername(std::string input, const std::vector<User *> & users)
{
    if (input.size() < 5)
    {
        throw std::invalid_argument("\t! The username is too short. (at least 5 characters)");
    }
    if (((input[0] - '0') > -1) && ((input[0] - '0') < 10))
    {
        throw std::invalid_argument("\t! The first character can't be a number.");
    }
    for (int i = 0; i < input.size(); i++)
    {
        if (!(((int(input[i]) < 58) && (int(input[i]) > 46)) || ((int(input[i]) < 123) && (int(input[i]) > 96))))
        {
            throw std::invalid_argument("\t! Invalid character(s).");
        }
    }
    std::string keyWords = input;
    std::transform(keyWords.begin(), keyWords.end(), keyWords.begin(), ::tolower);
    if (keyWords == "login" || keyWords == "signup" || keyWords == "username" || keyWords == "biography" || keyWords == "country" || keyWords == "phonenumber" ||
        keyWords == "password" || keyWords =="header" || keyWords == "birthdate" || keyWords == "delete" || keyWords == "account" || keyWords == "profile" || keyWords == "tweet" || keyWords == "dislike" || keyWords == "logout")
    {
        throw std::invalid_argument("\t! This is a reserved keyword. You can not choose it as your username.");
    }
    for (User * duplicateUser : users)
    {
        if (duplicateUser->getUsername() == input)
        {
            throw std::invalid_argument("\t! The username is taken.");
        }
    }
    this->username = input;
}

void User::setName(std::string input)
{
    this->name = input;
}

void User::setBiography(std::string input)
{
    if (input.size() > 160)
    {
        throw std::invalid_argument("\t! The biography is too long. (Up to 160 characters)");
    }
    this->biography = input;
}

void User::setCountry(std::string input)
{
    this->country = input;
}

void User::setLink(std::string input)
{
    if ((input.size() > 0) && (input.substr(0, 8) != "https://"))
    {
        this->link = "https://" + input;
    }
    else
    {
        this->link = input;
    }
}

void User::setPhoneNumber(std::string input, const std::vector<User *> & users)
{
    for(int i = 0; i < input.size(); i++)
    {
        if(((input[i] - '0') < 0) || ((input[i] - '0') > 9)) 
        {
            throw std::invalid_argument("\t! The phone number isn't valid.");
        }
    }
    if ((input.size() == 10) && (input[0] == '9'))
    {
        input = "98" + input;
    }
    else if ((input.size() == 11) && (input.substr(0, 2) == "09"))
    {
        input = "98" +  input.substr(1, 10);
    }
    else if (!((input.size() == 12) && (input.substr(0, 3) == "989")))
    {
        throw std::invalid_argument("\t! The phone number isn't valid.");
    }
    for (User * duplicateUser : users)
    {
        if (duplicateUser->getPhoneNumber() == input)
        {
            throw std::invalid_argument("\t! This phone number is used.");
        }
    }
    this->phoneNumber = input;
}

void User::setPassword(const std::string & input)
{
    for (int i = 0; i < input.size(); i++)
    {
        if (input[i] == ' ')
        {
            throw std::invalid_argument("\t! Invalid character(s).");
        }
    }
    SHA256 sh;
    this->password = sh(input);
}

void User::setHeader(std::string input)
{
    std::transform(input.begin(), input.end(), input.begin(), ::tolower);
    if (input == "white" || input == "red" || input == "orange" || input == "yellow" || input == "pink" ||
        input == "green" || input == "blue" || input == "purple" || input == "black")
    {
        this->header = input;
    }
    else if (input == "")
    {
        this->header = "white";
    }
    else
    {
        throw std::invalid_argument("\t! The header is not valid!");
    }
}

void User::setBirthDay(int dd)
{
    birthDate.setDay(dd);
}

void User::setBirthMonth(int mm)
{
    birthDate.setMonth(mm);
}

void User::setBirthYear(int yy)
{
    birthDate.setYear(yy);
}

std::string User::getUsername() const
{
    return this->username;
}

std::string User::getName() const
{
    return this->name;
}

std::string User::getBiography() const
{
    return this->biography;
}

std::string User::getCountry() const
{
    return this->country;
}

std::string User::getLink() const
{
    return this->link;
}

std::string User::getPhoneNumber() const
{
    return this->phoneNumber;
}

std::string User::getPassword() const
{
    return this->password;
}

std::string User::getHeader() const
{
    return this->header;
}

std::string User::getBirthDate() const
{
    return this->birthDate.printDate();
}

std::string User::showColor() const
{
    std::string Header = this->header;
    if (Header == "red")
    {
        return RED;
    }
    else if (Header == "orange")
    {
        return ORANGE;
    }
    else if (Header == "yellow")
    {
        return YELLOW;
    }
    else if (Header == "pink")
    {
        return PINK;
    }
    else if (Header == "green")
    {
        return GREEN;
    }
    else if (Header == "blue")
    {
        return BLUE;
    }
    else if (Header == "purple")
    {
        return PURPLE;
    }
    else if (Header == "black")
    {
        return BLACK;
    }
    return "";
}

std::string User::getJoinDate() const
{
    return this->joinDate.printDate();
}

bool User::isOld() const
{
    return this->birthDate.isOldEnough();
}

void User::deleteTweet(int num)
{
    this->vecTweet[num] = nullptr;
}

void User::changeTweet(int num, std::string & str)
{ 
    vecTweet[num - 1]->setText(str); 
}

std::vector<Tweet *> User::getVecTweet()
{
    return this->vecTweet;
}

void User::setVec(std::string & st)
{
    Tweet newTweet;
    newTweet.setText(st);
    this->vecTweet.push_back(new Tweet(newTweet));
}
