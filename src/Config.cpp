#include <iostream>
#include <string>

#include "Config.hpp"

void setString(std::string & input) //
{
    int pos = 0;
    if ((pos = input.find("\"")) != std::string::npos)
    {
        while ((input.find("   ") != std::string::npos) && (input.find("   ") < pos))
        {
            input.erase(input.find("   "), 2);
            pos = input.find("\"");
        }
        while ((input.find("  ") != std::string::npos) && (input.find("  ") < pos))
        {
            input.erase(input.find("  "), 1);
            pos = input.find("\"");
        }
        pos = input.find_last_of("\"");
        while ((input.find_last_of("   ") != std::string::npos) && (input.find_last_of("   ") > pos))
        {
            input.erase(input.find_last_of("   "), 2);
        }
        while ((input.find_last_of("  ") != std::string::npos) && (input.find_last_of("  ") > pos))
        {
            input.erase(input.find_last_of("  "), 1);
            pos = input.find_last_of("\"");
        }
    }
    else
    {
        while (input.find("   ") != std::string::npos)
        {
            input.erase(input.find("   "), 2);
        }
        while (input.find("  ") != std::string::npos)
        {
            input.erase(input.find("  "), 1);
        }
    }

    if ((input.size() > 0) && (input[0] == ' '))
    {
        input.erase(0, 1);
    }
    if ((input.size() > 0) && (input[input.size() - 1] == ' '))
    {
        input.erase(input.size() - 1, 1);
    }
    if ((input[0] != '@') || (input.find(':') != std::string::npos))
    {
        int pos = input.find('@');
        while (input.find('@', pos) != std::string::npos)
        {
            if ((input.find('@', pos) < input.find("\"")) || (input.find('@', pos) > input.find_last_of("\""))) input.erase(input.find('@', pos), 1);
            pos = input.find('@', pos + 1);
        }
    }
}
